package com.SamePlacesCommunity.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.thirdapp.R;
import com.zhy.socket.ChatMsgEntity;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SimpleAdapter.ViewBinder;

public class TagPushActivity extends Activity implements OnClickListener 
{
	private static final int PORT = 7714;
	private static final String SERVERADD = "10.14.4.163";
	private ImageView SendBingoRequest;
	private Button Selectvisible;
	private EditText SendBingoInfo;
	private Bundle b;
	private ListView Taglistview;
	private MyHandler myhandler;
	private Thread ConnectThread;
	private Thread SendTagsThread;
	private Thread PopKeepAlive;
	private Runnable ConnectThreadRunnable;
	private Runnable SendTagsThreadRunnable;
	private Runnable PopKeepAliveRunnable;
	private Socket s;
	private DataOutputStream dos;
	private DataInputStream dis;
	
	//private information
	private String tags;
	private String username="wakeup";
	private String userid="320908234";
	private String userlocation = "保定";
	private int checkboxposition[];
	private String checkedtagsinfo[];
	private Bundle infobundle;
	private String TAGS[][]={{"保定","保定真人CS","保定出发泰山祈福游","环保定骑车","保定骑车","保定汉丽轩"},
			  {"北京","故宫一日游","海底捞北京店","北京古玩街","北京希尔顿大酒店"},
			  {"上海","上海希尔顿大酒店预定","上海城隍庙小吃街","上海大娘水饺"},
			  {"深圳","深圳吁胎龙虾馆","深圳维港茶餐厅","深圳世界之窗主题公园","深圳腾讯总部"}};
	public static int MODE = MODE_PRIVATE;//定义访问模式为私有模式
    public static final String PREFERENCE_NAME = "UserInfo";//设置保存时的文件的名称
	private static List<Map<String,Object>> tagslist = new ArrayList<Map<String , Object>>();
	private boolean ReceiveFlag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tag_push);
		initView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tag_push, menu);
		return true;
	}
	
	
	
	  private void initView(){
		  ReceiveFlag= true;		  
		  Taglistview = (ListView)findViewById(R.id.Tagsforchoose);
		  SendBingoInfo = (EditText)findViewById(R.id.BingoInfoSent);
		  SendBingoRequest = (ImageView) findViewById(R.id.SendBingoRequestPic);
		  SendBingoRequest.setOnClickListener(this);
		  myhandler = new MyHandler();
		  SendBingoInfo.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO OnTextChanged
				Log.v("is running","running");
			    tagslist.clear();
				String inputtag;
				
					if(SendBingoInfo.getText().toString()==null||SendBingoInfo.getText().toString()==""||SendBingoInfo.getText().toString()==" ")
						inputtag = "";
					
					else 
						inputtag=SendBingoInfo.getText().toString();
				
				boolean flag = false;//标志位，判断用户输入是否为空
				if (inputtag ==""||inputtag == null)
					flag = false;
				else 
					flag = true;
				if(flag){
					
					EditText SendBingoInfo = (EditText)findViewById(R.id.BingoInfoSent);
					inputtag = SendBingoInfo.getText().toString();
				    
					StartSendTagThread(inputtag);
					if(flag == true){
					for(int i = 0;i<TAGS.length;i++)
					{
						if(inputtag.equals(TAGS[i][0])){
							for(int j=0;j<TAGS[i].length;j++){
								setData(TAGS[i][j]);
							    }
						    }
					    }
//					TagPushActivity.this.myhandler.sendEmptyMessage(1);
					}
					if(tagslist!=null)
					addView();
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
								
			}
		});
		  
	      
	      StoreUserInfo();
	      infobundle = GetUserInfo();
		  StartConnectThread();
//		  StartPopKeepAlive();
	
	 }
	  
	  private Bundle GetUserInfo() {
		// TODO Auto-generated method stub
		  SharedPreferences sharedpreferences = this.getSharedPreferences(PREFERENCE_NAME,MODE);
		  String name = sharedpreferences.getString("Name", "NAN");
	      String userid = sharedpreferences.getString("UserId", "NAN");
	      String location = sharedpreferences.getString("Location", "NAN");
	      Bundle b = new Bundle();
	      b.putString("Name", name);
	      b.putString("UserId", userid);
	      b.putString("Location", location);
	      return b;
	}

	private void StoreUserInfo() {
		// TODO Auto-generated method stub
		  SharedPreferences sharedpreferences = this.getSharedPreferences(PREFERENCE_NAME, MODE);
	      SharedPreferences.Editor editor = sharedpreferences.edit();
	      editor.putString("Name", username);
	      editor.putString("UserId", userid);
	      editor.putString("Location", userlocation);
	      editor.commit();
	}

	private void StartSendTagThread(final String inputtag) {
		// TODO StartSendTagThread
		  SendTagsThreadRunnable = new Runnable() {
              public void run() {
                  System.out.println("running!");
                  
                  try {
                	  if(s!=null&&s.isConnected())
						SendTag(inputtag,userid);
					} catch (IOException e) {
						
						e.printStackTrace();
					}  
                  TagPushActivity.this.myhandler.sendEmptyMessage(3);
              }
          };
		  SendTagsThread =new Thread (SendTagsThreadRunnable); 
		  SendTagsThread.start();
		  
	}

	private void StartPopKeepAlive() {
		
		  PopKeepAliveRunnable = new Runnable() {
				
				@Override
				public void run() {
					while(true){
					// TODO StartPopThread
					if(!s.isConnected()){
						Connect();
						try {
							HeartPop();
							Thread.sleep(10000);
						    } catch (IOException e) {
							
							e.printStackTrace();
						    } catch (InterruptedException e) {
						
						    e.printStackTrace();
							}
						
					    }else{
					    	try {
								HeartPop();
								Thread.sleep(10000);
							} catch (IOException e) {
								
								e.printStackTrace();
							} catch (InterruptedException e) {
								
								e.printStackTrace();
							}
							
					    }
					TagPushActivity.this.myhandler.sendEmptyMessage(2);
				    }
				}
			};
        PopKeepAlive = new Thread(PopKeepAliveRunnable);
        PopKeepAlive.start();
	}

	private void StartConnectThread() {
		
		  ConnectThreadRunnable = new Runnable() {
				
				@Override
				public void run() {
					// TODO StartConnectThread
					Connect();
					TagPushActivity.this.myhandler.sendEmptyMessage(1);//连接执行后发送消息给主线程
					
				}
			};
			ConnectThread = new Thread(ConnectThreadRunnable);
			ConnectThread.start();
	}

	/*
	   * 线程的run被执行完之后线程自动销毁，会被回收掉*/
	  public void ConnectAndSendTagForMatch(String tag,String userid) throws IOException {
			Connect();
			if(s!=null&&s.isConnected())
			SendTag(tag,userid);
	    }
	  public boolean isNetWorkAvai(){
		    ConnectivityManager cm=(ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
<<<<<<< HEAD
		    NetworkInfo ni= (NetworkInfo)cm.getActiveNetworkInfo();
		    NetworkInfo ni_wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		    boolean iswificon=ni_wifi.isConnected();
		    if(ni!=null){
		    boolean ismobileco = (ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_CDMA||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_EDGE||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_GPRS||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_LTE);
	        return(ismobileco||iswificon);
	        }else{
	        	return  iswificon;
	        }
		    
	  } 
=======
		    NetworkInfo ni_mobile=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	        boolean ismobilecon=ni_mobile.isConnected();
	        NetworkInfo ni_wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	        boolean iswificon=ni_wifi.isConnected();
	        return(ismobilecon||iswificon);
	  }
>>>>>>> e2858b2f1935b06f340b3f1c35841a4e1b97fc2b
      public void Connect(){
    	    
	        if(isNetWorkAvai()){
	        
	        try {
	        	s= new Socket();
	        	SocketAddress sa = new InetSocketAddress(SERVERADD, PORT);
	        	s.connect(sa, 3000);
	        
	            if(!s.isConnected()){
	            	Toast.makeText(this, "服务器没有响应，连接失败", 1500).show();
	            }
	            else if(s.isConnected()){
	            	dos = new DataOutputStream (s.getOutputStream());
	            	dis = new DataInputStream (s.getInputStream());
	            	
	            }
	         }catch (UnknownHostException e) {
	              System.out.println("服务器未找到");
	         
	              e.printStackTrace();
	          }catch (SocketTimeoutException  e) {
	              System.out.println("连接超时");
	              
	              e.printStackTrace();
	         }catch (IOException e) {
	              System.out.println("锟紹锟斤拷失锟斤拷");
	              e.printStackTrace();
	         }
	        }else{
//	        	Toast.makeText(this, "你的网络好像有问题哦，请检查网络设置", 1500).show();
	        	}
      }
      
	  public void SendTag(String tag,String userid) throws IOException {
		// TODO Auto-generated method stub
		    Log.v("SendTagCalled","true");
		    String fetchtags = "FetchTags"+"#"+userid+"#"+tags;
		    if(s.isConnected())
		    {dos.writeUTF(fetchtags); dos.flush();}
		    else Log.v("SocketState", "false");
		    //TODO 这里的SendTag方法后面的方法有所不同    
	}
	  
	  public void HeartPop() throws IOException{
		  if(s.isConnected())
		    dos.writeInt(1);
		  else Log.v("SocketState", "false");
	  }
	  private void ReceiveReturnMessageThread(){
		 
		  if(s!=null&&s.isConnected()){
			Runnable ReceiveReturnThread = new Runnable() {
		        public void run() {
		        	while(ReceiveFlag){
		            System.out.println("running!");
		             ReceiveMsg();
		             ReceiveFlag = false;
		        	}
		        }
		    };  
		    Thread temp = new Thread(ReceiveReturnThread);
		    temp.start();
		    }else {
		    	Toast.makeText(getApplicationContext(), "无法连接服务器", 1500).show();
		    }
		}

	   
	  private void ReceiveMsg() {
	        if (s.isConnected()) {
	            try {
	            	String reMsg = "";
	                while ((reMsg = dis.readUTF()) != null) {
	                    System.out.println(reMsg);
	                    if (reMsg.startsWith("ReturnTags")) {
	                    	//TODO 需要改动 接受tag
	                    	String s1[]=reMsg.split("#");
	                    	setData(s1);
	                    	
	                    	boolean F=true;
//	                    	ChatMsgEntity newMessage = new ChatMsgEntity(ID+"说:", date, context, R.layout.list_say_me_item);

	                        try {
	                           
	                            TagPushActivity.this.myhandler.sendEmptyMessage(-1);                          
	                            Thread.sleep(100);
	                        } catch (InterruptedException e) {
	                            // TODO Auto-generated catch block
	                            e.printStackTrace();
	                        }
	                    }
	                }
	            } catch (SocketException e) {
	                // TODO: handle exception
	                System.out.println("exit!");
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }

	        }else{
	        	Toast.makeText(getApplicationContext(), "与服务器断开链接", 1500).show();
	        }
	    
	  }

	private void setData(String tag) {
		 HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("Tags",tag);
			tagslist.add(map);
		// TODO SetListData
		
	}
	private void setData(String tag[]){
		HashMap<String, Object> map = new HashMap<String, Object>();
		for(int i=0;i<tag.length;i++){
			map.put("Tags", tag[i]);
		}
		tagslist.add(map);
	}

	public void addView() {
    	SimpleAdapter adapter = new SimpleAdapter(
 			   this,getData(),
 			   R.layout.new_bingo_adapter_listview,
				   new String[]{"Tags"},
				   new int[]{R.id.Tags}
 			   );
 	
 	adapter.setViewBinder(new ViewBinder(){

			@Override
			public boolean setViewValue(View view, Object data,
					String textRepresentation) {
				if(view instanceof ImageView && data instanceof Bitmap){
					ImageView iv = (ImageView) view;
					iv.setImageBitmap((Bitmap)data);
					return true;
				}else{
				// TODO SetBitmapViewBinder
				return false;
				}
			}
 		
 	});
 	
 	
 	
 	Taglistview.setAdapter(adapter);
 	//int count = Taglistview.getChildCount();
 	checkboxposition =new int[10];
 	checkedtagsinfo = new String[10];
 	Taglistview.setOnItemClickListener(new OnItemClickListener() {
 		
 		
		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
			
			LinearLayout l =(LinearLayout) Taglistview.getChildAt(position);
			TextView t = (TextView)l.findViewById(R.id.Tags);
			CheckBox cb = (CheckBox)l.findViewById(R.id.choosetags);
			cb.toggle();
			if(cb.isChecked()){
				checkboxposition[position]=1;
				checkedtagsinfo[position]=t.getText().toString();
				}
			else {
				checkboxposition[position]=0;
				checkedtagsinfo[position]=null;
				}
			//获得选中项的HashMap对象
			// 取得ViewHolder对象，这样就省去了通过层层的findViewById去实例化我们需要的cb实例的步骤
			
//	　　　　　　　　　　ViewHolder holder = (ViewHolder) arg1.getTag();
//	                // 改变CheckBox的状态
//	                holder.cb.toggle();
//	                // 将CheckBox的选中状况记录下来
//	                MyAdapter.getIsSelected().put(arg2, holder.cb.isChecked()); 
//			
			
			
			
			
			// TODO Auto-generated method stub
			
		}
		 
	});
		
	}
	private List<Map<String , Object>> getData() {
		return tagslist;
	}
	@Override
    protected void onDestroy() {
    	//退出时销毁定位
		
        try {
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("服务器错误");
		}
		tagslist.clear();
        super.onDestroy();
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

        case R.id.SendBingoRequestPic:
        	
        	Intent i = new Intent(getApplicationContext(), SetLoAndPriActivity.class);
        	i.putExtra("TAGS", checkedtagsinfo);
        	i.putExtra("USERNAME", username);
        	i.putExtra("USERID", userid);
        	i.putExtra("choosedfriendposition", "");
        	i.putExtra("choosedfriendnames", "");
        	startActivity(i);
        	
        	break;
        
         default:
             break;
     }
	}

	private String GetTags() {
		
		return SendBingoInfo.getText().toString();
		
		
	}

//	private void SendBingoRequest(String tags, String username, String userid,int infovisibility) {
		
//		Bundle b = new Bundle();
//		b.putStringArray("TAGS", checkedtagsinfo);
//		b.putString("USERNAME", username);
//		b.putString("USERID", userid);
//		
//	}

	class MyHandler extends Handler{
    	public MyHandler(){
    		
    	}
    	public MyHandler(Looper L){
    		super(L);
    	}
    	@Override
    	
    	public void handleMessage(Message msg){
    		super.handleMessage(msg);
    		switch (msg.what){
    		// TODO 1启动连接 2 心跳 3发送Tag -1 获取并addView
    		case 1:
    			Log.v("Connected", "true");
    			if(s!=null&&s.isConnected())
    			ReceiveReturnMessageThread();
    			break;
    		case 2:
    			Log.v("Poped", "true");
    			break;
    		case 3:
    			Log.v("Sent", "true");
    			break;
    		case -1:
    			addView();
    		    break;
    		
    	}
      }
    }


}
