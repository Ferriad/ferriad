package com.SamePlacesCommunity.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.SamePlacesCommunity.Main.SetLoAndPriActivity.MyHandler;
import com.example.thirdapp.R;
import com.example.thirdapp.R.layout;
import com.example.thirdapp.R.menu;
import com.zhy.socket.ChatMsgEntity;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter.ViewBinder;

public class ChooseAtFriend extends Activity implements OnClickListener{
private ListView atFriendList;
private Button confirm;
private Button cancel;
private String choosedfriend[];
private int  checkboxposition[];
private String[] checkeduserinfo;
private static List<Map<String,Object>> list;
private String userid;
private String username;
private String tagaftertrans;
private int count;
//network
private static final int PORT = 7714;
private static final String SERVERADD = "10.14.4.163";
private Socket s;
private DataOutputStream dos;
private DataInputStream dis;
private MyHandler myhandler;
private boolean ReceiveFlag;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_at_friend);
		init();
		
	}

	private void init() {
		// TODO Auto-generated method stub
		Intent i = getIntent();
		tagaftertrans = i.getStringExtra("TAGS");
		checkboxposition =new int[50];
	 	checkeduserinfo = new String[50];
		username = i.getStringExtra("USERNAME");
		userid = i.getStringExtra("USERID");
		atFriendList = (ListView)findViewById(R.id.atFriendList);
		confirm = (Button)findViewById(R.id.confirm);
	    confirm.setOnClickListener(this);
		cancel = (Button)findViewById(R.id.cancel);
		cancel.setOnClickListener(this);
		myhandler = new MyHandler();
		ReceiveFlag=true;
		list = new ArrayList<Map<String , Object>>();
		String usernames[]={"李菲儿","囧哥","谢可可","Elna","李袁熙","胡杨娜","Eric","Ferriad","Sophia","刘点点","Yeye"};
		for(int j=0;j<usernames.length;j++)
		setData(usernames[j]);
		addView();
		StartConnectThread();
		ReceiveReturnMessageThread();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.choose_at_friend, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.confirm:
			Intent i1 = new Intent(this, BingoMessageDisplayActivity.class);
			i1.putExtra("choosedfriendposition",checkboxposition);
			i1.putExtra("choosedfriendnames", choosedfriend);
			i1.putExtra("TAGS", tagaftertrans);
        	i1.putExtra("USERNAME", username);
        	i1.putExtra("USERID", userid);
        	i1.putExtra("ATFRIEND", checkeduserinfo);
			startActivity(i1);
			ChooseAtFriend.this.finish();
			break;
		case R.id.cancel:
			Intent i2 = new Intent(this, SetLoAndPriActivity.class);
			i2.putExtra("choosedfriendposition","");
			i2.putExtra("choosedfriendnames", "");
			i2.putExtra("TAGS", tagaftertrans);
			i2.putExtra("USERNAME", username);
			i2.putExtra("USERID", userid);
			startActivity(i2);
			ChooseAtFriend.this.finish();
			break;
		}
	}
	public void addView() {
    	SimpleAdapter adapter = new SimpleAdapter(
 			   this,getData(),
 			   R.layout.new_friend_at_adapter,
				   new String[]{"USERNAMES"},
				   new int[]{R.id.usersname}
 			   );
// 	
// 	adapter.setViewBinder(new ViewBinder(){
//
//			@Override
//			public boolean setViewValue(View view, Object data,
//					String textRepresentation) {
//				if(view instanceof ImageView && data instanceof Bitmap){
//					ImageView iv = (ImageView) view;
//					iv.setImageBitmap((Bitmap)data);
//					return true;
//				}else{
//				// TODO Auto-generated method stub
//				return false;}
//			}
// 		
// 	});
 	
 	
 	
 	atFriendList.setAdapter(adapter);
 	
 	
 	atFriendList.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
			
			LinearLayout l =(LinearLayout) atFriendList.getChildAt(position);
			TextView t = (TextView)l.findViewById(R.id.usersname);
			CheckBox cb = (CheckBox)l.findViewById(R.id.chooseuser);
			cb.toggle();
			if(cb.isChecked()){
				checkboxposition[position]=1;
				checkeduserinfo[position]=t.getText().toString();
				}
			else {
				checkboxposition[position]=0;
				checkeduserinfo[position]=null;
				}
			
			
			// TODO Auto-generated method stub
			
		}
		 
	});
	}
	 private String TransTags(String tag []){
	    	String TagsAfterTrans = "";
	    	for(int i=0;i<tag.length;i++){
	    		if(tag[i]!=null)
	    			TagsAfterTrans+=tag[i]+" ";
	    	}
	    	return TagsAfterTrans;
	    }
	private List<Map<String , Object>> getData() {
		return list;
	}

	private void setData(String USERNAMES){
	    HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("USERNAMES",USERNAMES);
		list.add(map);
    }
	 @Override
	    protected void onDestroy() {
	    	//退出时销毁定位、停止接受线程
	        ReceiveFlag = false;
		    list.clear();
	        super.onDestroy();
	    }
	 private void StartSendTagThread(final String inputtag,final String userid) {
			// TODO StartSendTagThread
			  Runnable SendTagsThreadRunnable = new Runnable() {
	              public void run() {
	                  System.out.println("running!");
	                  
	                  try {
							SendTag(inputtag,userid);
						} catch (IOException e) {
							
							e.printStackTrace();
						}  
	                  ChooseAtFriend.this.myhandler.sendEmptyMessage(1);
	              }
	          };
			  Thread SendTagsThread =new Thread (SendTagsThreadRunnable); 
			  SendTagsThread.start();
			  
		}

		private void StartPopKeepAlive() {
			
			  Runnable PopKeepAliveRunnable = new Runnable() {
					
					@Override
					public void run() {
						while(true){
						// TODO StartPopThread
						if(!s.isConnected()){
							Connect();
							try {
								HeartPop();
								Thread.sleep(10000);
							    } catch (IOException e) {
								
								e.printStackTrace();
							    } catch (InterruptedException e) {
							
							    e.printStackTrace();
								}
							
						    }else{
						    	try {
									HeartPop();
									Thread.sleep(10000);
								} catch (IOException e) {
									
									e.printStackTrace();
								} catch (InterruptedException e) {
									
									e.printStackTrace();
								}
								
						    }
		                  ChooseAtFriend.this.myhandler.sendEmptyMessage(1);
					    }
					}
				};
	        Thread PopKeepAlive = new Thread(PopKeepAliveRunnable);
	        PopKeepAlive.start();
		}

		private void StartConnectThread() {
			
			 Runnable ConnectThreadRunnable = new Runnable() {
					
					@Override
					public void run() {
						// TODO StartConnectThread
						Connect();
						
					}
				};
				Thread ConnectThread = new Thread(ConnectThreadRunnable);
				ConnectThread.start();
		}
		 private void ReceiveReturnMessageThread(){
			  int count = 0;
			  if(s!=null&&s.isConnected()){
				Runnable ReceiveReturnThread = new Runnable() {
			        public void run() {
			        	while(ReceiveFlag){
			            System.out.println("running!");
			             ReceiveMsg();
			             ReceiveFlag = false;
			        	}
			        }
			    };  
			    Thread temp = new Thread(ReceiveReturnThread);
			    temp.start();
			    }else if(count==2)
			    {Toast.makeText(getApplicationContext(), "无法连接服务器", 1500).show();}
			}

		 private void ReceiveMsg() {
		        if (s!=null&&s.isConnected()) {
		            try {
		            	String reMsg = "";
		                while ((reMsg = dis.readUTF()) != null) {
		                    System.out.println(reMsg);
		                    if (reMsg.startsWith("ReturnTags")) {
		                    	String s1[]=reMsg.split("#");
//		                    	String ID=s1[1];
//		                    	String date=s1[2];
//		                    	String context=s1[3];
//		                    	
		                    	boolean F=true;
		          
		                        try {
		                            Message msgMessage = new Message();                           
		                            msgMessage.what = -1;
		                            ChooseAtFriend.this.myhandler.sendMessage(msgMessage);                          
		                            Thread.sleep(1000);
		                        } catch (InterruptedException e) {
		                            // TODO Auto-generated catch block
		                            e.printStackTrace();
		                        }
		                    }
		                }
		            } catch (SocketException e) {
		                // TODO: handle exception
		                System.out.println("exit!");
		            } catch (IOException e) {
		                // TODO Auto-generated catch block
		                e.printStackTrace();
		            }

		        }else{
		        	Message m = new Message();
		        	m.what=4;
		        	ChooseAtFriend.this.myhandler.sendMessage(m);
		        }
		    
		  }

		/*
		   * 线程的run被执行完之后线程自动销毁，会被回收掉*/
		  public void ConnectAndSendTagForMatch(String tag,String userid) throws IOException {
				Connect();
				SendTag(tag,userid);
		    }
<<<<<<< HEAD
		  public boolean isNetWorkAvai(){
			  
			    ConnectivityManager cm=(ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
			    NetworkInfo ni= (NetworkInfo)cm.getActiveNetworkInfo();
			    NetworkInfo ni_wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			    boolean iswificon=ni_wifi.isConnected();
			    if(ni!=null){
			    boolean ismobileco = (ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_CDMA||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_EDGE||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_GPRS||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_LTE);
		        return(ismobileco||iswificon);
		        }else{
		        	return  iswificon;
		        }
			    
		  } 
	      public void Connect(){
	    	    
		        if(isNetWorkAvai()){
=======
	      public void Connect(){
	    	    ConnectivityManager cm=(ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
				NetworkInfo ni_mobile=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		        boolean ismobilecon=ni_mobile.isConnected();
		        NetworkInfo ni_wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		        boolean iswificon=ni_wifi.isConnected();
		        
		        if(ismobilecon||iswificon){
>>>>>>> e2858b2f1935b06f340b3f1c35841a4e1b97fc2b
		        
		        	try {
			        	s= new Socket();
			        	SocketAddress sa = new InetSocketAddress(SERVERADD, PORT);
			        	s.connect(sa, 3000);
			        
			            if(!s.isConnected()){
			            	Toast.makeText(this, "服务器没有响应，连接失败", 1500).show();
			            }
			            else if(s.isConnected()){
			            	dos = new DataOutputStream (s.getOutputStream());
			            	dis = new DataInputStream (s.getInputStream());
			            	
			            }
			         }catch (UnknownHostException e) {
			              System.out.println("服务器未找到");
			         
			              e.printStackTrace();
			          }catch (SocketTimeoutException  e) {
			              System.out.println("连接超时");
			              
			              e.printStackTrace();
			         }catch (IOException e) {
			              System.out.println("锟紹锟斤拷失锟斤拷");
			              e.printStackTrace();
			         }
			        }else{
//			        	Toast.makeText(this, "你的网络好像有问题哦，请检查网络设置", 1500).show();
			        	}
	      }
	      
		  public void SendTag(String tag, String userid2) throws IOException {
			// TODO Auto-generated method stub
			    Log.v("SendTagCalled","true");
			    String fetchfriendslist = "FetchFriends"+"#"+userid2+"#"+tag;
			    if(s.isConnected())
				dos.writeUTF(fetchfriendslist);
			    else Log.v("SocketState", "false");
		}
		  
		  public void HeartPop() throws IOException{
			  if(s.isConnected())
			    dos.writeInt(1);
			  else Log.v("SocketState", "false");
		  }
		  class MyHandler extends Handler{
		    	public MyHandler(){
		    		
		    	}
		    	public MyHandler(Looper L){
		    		super(L);
		    	}
		    	@Override
		    	
		    	public void handleMessage(Message msg){
		    		super.handleMessage(msg);
		    		switch (msg.what){
		    		// TODO 1启动连接 2 心跳 3发送Tag -1 获取并addView
		    		case 1:
		    			Log.v("Connected", "true");
		    			break;
		    		case 2:
		    			Log.v("Poped", "true");
		    			break;
		    		case 3:
		    			Log.v("Sent", "true");
		    			break;
		    		case 4:
		    			Log.v("Receive","true");
		    		    break;
		    		case -1:
		    			break;
		    	}
		      }
		    }

}
