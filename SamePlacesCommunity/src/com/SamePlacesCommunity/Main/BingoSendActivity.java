package com.SamePlacesCommunity.Main;

import com.example.thirdapp.R;
import com.example.thirdapp.R.layout;
import com.example.thirdapp.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class BingoSendActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bingo_send);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bingo_send, menu);
		return true;
	}

}
