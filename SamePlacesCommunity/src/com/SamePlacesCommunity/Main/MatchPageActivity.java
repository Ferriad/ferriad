package com.SamePlacesCommunity.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import com.SamePlacesCommunity.Main.SetLoAndPriActivity.MyHandler;
import com.example.thirdapp.R;
import com.example.thirdapp.R.layout;
import com.example.thirdapp.R.menu;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MatchPageActivity extends Activity {
	private static final int PORT = 7714;
	private static final String SERVERADD = "10.14.4.163";
	private Socket s;
	private DataOutputStream dos;
	private DataInputStream dis;
	private MyHandler myhandler;
	private int info_id;
	private int user_id;
	private String user_name;
    private String[] info_title={"北京古玩街","上海大娘水饺"};
    private String[] user_names={"贫僧自东土大唐来","一周立波"};
    private String[] info_index_array = {"北京古玩街","上海大娘水饺"};
    private String[] info = {"据介绍，琉璃厂文化产业园区将建成中国最大的珠宝古玩经营区、传统手工艺品集散区、传统文化交流活动中心区和古旧书籍及文化用品名家名店聚集区。届时，琉璃厂将让海内外游客充分领略老北京悠久的传统文化底蕴，成为荟萃北京胡同文化、四合院文化、园林文化、楹联文化、古建筑文化的大型文化博物馆。据了解，规划中的琉璃厂文化产业园区东起延寿寺街，西至南柳巷，总面积１平方公里，其中四分之一部分为古文化用品商业区，四分之三为景观区。 琉璃厂位于北京宣武区和平门外，整个街道全长约１０００米，迄今已有３００多年的历史。元、明两代为修建宫殿皇城，在这里设置琉璃砖瓦窑，琉璃厂由此得名。清初古董商开始在此经营，乾隆时已成为古玩字画、古籍碑帖及文房四宝的集散地。 驰名中外的荣宝斋及中国书店和文物商店的许多门市部如文奎堂、邃雅斋、宝古斋、庆云堂等先后在此设立。。建成的琉璃厂将保留原有仿古建筑，并新建一些四合院，以展现老北京的历史文化风貌。"
    		,"大娘水饺于1996年4月创建于常州，是以水饺为主打产品，融合西式快餐理念，致力于价格平民化、品质标准化、管理现代化、品牌国际化的中式快餐连锁企业。经过全体员工十年多的共同努力和开拓创新，先后在苏、沪、皖、京、鲁、浙、粤、赣、甘、黑等地区开办了逾300家连锁店，每年光顾“大娘水饺”的消费者超过了5000万人次。2002年11月和2004年12月，分别在印尼雅加达与澳大利亚悉尼成功开办连锁店。自2003年起，又相继投资1亿元成立了一家速冻食品生产企业。"};
    private TextView title;
    private TextView info_text;
    private TextView info_username;
    private Button btn_zudui;
    private Button btn_fanfui;
    
    private boolean ReceiveFlag;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent i = getIntent();
		String id = i.getStringExtra("INFOID");
		
		setContentView(R.layout.activity_match_page);
		init(id);
	}
	@Override
	protected void onDestroy(){
		ReceiveFlag = false;
		super.onDestroy();
	}

	private void init(String ID) {
		int id = Integer.parseInt(ID);
		id = id - 1;
		ReceiveFlag = true;
		// TODO Auto-generated method stub
		title = (TextView)findViewById(R.id.Title);
		info_text =(TextView)findViewById(R.id.Infotext);
		info_username = (TextView)findViewById(R.id.InfoUsername);
		btn_zudui= (Button)findViewById(R.id.Zudui);
		btn_zudui.setOnClickListener(new OnclickListener());
		btn_fanfui=(Button)findViewById(R.id.Fanhui);
		btn_fanfui.setOnClickListener(new OnclickListener());
		Log.v("info", info_title[id]);
		String info_titleString = info_title[id];
		String info_textString = info[id];
		String info_userString = user_names[id];
		info_username.setText("发布人"+":"+info_userString);
		
		title.setText(info_titleString);
		info_text.setText(info_textString);
		StartConnectThread();
		ReceiveReturnMessageThread();
		
	}
	class OnclickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch(v.getId()){
			case R.id.Zudui:
				SendZuiduiRequest(user_id,user_name,info_id);
				break;
				
			case R.id.Fanhui:
				
				break;
				}
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.match_page, menu);
		return true;
	}
	
	public void SendZuiduiRequest(int user_id2, String user_name2,
			int info_id2) {
		// TODO Auto-generated method stub
		final String SendTransUTF = "SendZuduiRequest"+"#"+user_id2+"#"+user_name2+"#"+info_id2;
		  Runnable SendZuiduiRequest = new Runnable() {
				
				@Override
				public void run() {
					
					// TODO StartPopThread
					if(s!=null&&s.isConnected()){
						
						try {
							dos.writeUTF(SendTransUTF);
							MatchPageActivity.this.myhandler.sendEmptyMessage(2);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					    }else{
					    	
							
					    }
					
				    
				}
			};
    Thread PopKeepAlive = new Thread(SendZuiduiRequest);
    PopKeepAlive.start();
	}

	private void StartPopKeepAlive() {
		
		  Runnable PopKeepAliveRunnable = new Runnable() {
				
				@Override
				public void run() {
					while(true){
					// TODO StartPopThread
					if(!s.isConnected()){
						Connect();
						try {
							HeartPop();
							Thread.sleep(10000);
						    } catch (IOException e) {
							
							e.printStackTrace();
						    } catch (InterruptedException e) {
						
						    e.printStackTrace();
							}
						
					    }else{
					    	try {
								HeartPop();
								Thread.sleep(10000);
							} catch (IOException e) {
								
								e.printStackTrace();
							} catch (InterruptedException e) {
								
								e.printStackTrace();
							}
							
					    }
					MatchPageActivity.this.myhandler.sendEmptyMessage(2);
				    }
				}
			};
      Thread PopKeepAlive = new Thread(PopKeepAliveRunnable);
      PopKeepAlive.start();
	}

	private void StartConnectThread() {
		
		 Runnable ConnectThreadRunnable = new Runnable() {
				
				@Override
				public void run() {
					// TODO StartConnectThread
					Connect();
					
				}
			};
			Thread ConnectThread = new Thread(ConnectThreadRunnable);
			ConnectThread.start();
	}

	/*
	   * 线程的run被执行完之后线程自动销毁，会被回收掉
	   * */
	  public void ConnectAndSendTagForMatch(String tag) throws IOException {
			Connect();
<<<<<<< HEAD
			if(s!=null&&s.isConnected())
			SendFeedId(feedid);
	    }
	  public boolean isNetWorkAvai(){
		  
		    ConnectivityManager cm=(ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
		    NetworkInfo ni= (NetworkInfo)cm.getActiveNetworkInfo();
		    NetworkInfo ni_wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		    boolean iswificon=ni_wifi.isConnected();
		    if(ni!=null){
		    boolean ismobileco = (ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_CDMA||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_EDGE||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_GPRS||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_LTE);
	        return(ismobileco||iswificon);
	        }else{
	        	return  iswificon;
	        }
		    
	  } 
	  public void Connect(){
  	      
	        if(isNetWorkAvai()){
=======
			SendTag(tag);
	    }
    public void Connect(){
  	    ConnectivityManager cm=(ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
			NetworkInfo ni_mobile=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	        boolean ismobilecon=ni_mobile.isConnected();
	        NetworkInfo ni_wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	        boolean iswificon=ni_wifi.isConnected();
	        
	        if(ismobilecon||iswificon){
>>>>>>> e2858b2f1935b06f340b3f1c35841a4e1b97fc2b
	        
	        try {
	        	s= new Socket();
	        	SocketAddress sa = new InetSocketAddress(SERVERADD, PORT);
	        	s.connect(sa, 3000);
	            
	            if(s.isConnected()){
	            	dos = new DataOutputStream (s.getOutputStream());
	            	dis = new DataInputStream (s.getInputStream());
	            	
	            }
	         }catch (UnknownHostException e) {
	              System.out.println("锟紹锟斤拷失锟斤拷");
	         
	              e.printStackTrace();
	          }catch (SocketTimeoutException  e) {
	              System.out.println("锟紹锟接筹拷锟絩锟斤拷锟斤拷锟斤拷锟斤拷未锟絖锟斤拷锟斤拷IP锟絜锟絗");
	              
	              e.printStackTrace();
	         }catch (IOException e) {
	              System.out.println("锟紹锟斤拷失锟斤拷");
	              e.printStackTrace();
	         }
	        }else{
//	        	Toast.makeText(this, "你的网络好像有问题哦，请检查网络设置", 1500).show();
	        	}
    }
    private void ReceiveReturnMessageThread(){
		  int count = 0;
		 
//		  while(count<3&&!s.isConnected()){
//			  if(s==null||!s.isConnected())
//			  Connect();
//			  count++;
//		  }
		  if(s!=null&&s.isConnected()){
			Runnable ReceiveReturnThread = new Runnable() {
		        public void run() {
		        	while(ReceiveFlag){
		            System.out.println("running!");
		             ReceiveMsg();
		             ReceiveFlag = false;
		        	}
		        }
		    };  
		    Thread temp = new Thread(ReceiveReturnThread);
		    temp.start();
		    }else if(count==2)
		    {Toast.makeText(getApplicationContext(), "无法连接服务器", 1500).show();}
		}

    private void ReceiveMsg() {
        if (s!=null&&s.isConnected()) {
            try {
            	String reMsg = "";
                while ((reMsg = dis.readUTF()) != null) {
                    System.out.println(reMsg);
                    if (reMsg.startsWith("ReturnTags")) {
                    	String s1[]=reMsg.split("#");
                    	String ID=s1[1];
                    	String date=s1[2];
                    	String context=s1[3];
                    	
                    	boolean F=true;
          
                        try {
                            Message msgMessage = new Message();                           
                            msgMessage.what = -1;
                            MatchPageActivity.this.myhandler.sendMessage(msgMessage);                          
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            } catch (SocketException e) {
                // TODO: handle exception
                System.out.println("exit!");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }else{
        	Message m = new Message();
        	m.what=4;
        	MatchPageActivity.this.myhandler.sendMessage(m);
        }
    
  }
    
	  public void SendTag(String tag) throws IOException {
		// TODO Auto-generated method stub
		    Log.v("SendTagCalled","true");
		    if(s.isConnected())
			dos.writeUTF(tag);
		    else Log.v("SocketState", "false");
	}
	  
	  public void HeartPop() throws IOException{
		  if(s.isConnected())
		    dos.writeInt(1);
		  else Log.v("SocketState", "false");
	  }
	  class MyHandler extends Handler{
	    	public MyHandler(){
	    		
	    	}
	    	public MyHandler(Looper L){
	    		super(L);
	    	}
	    	@Override
	    	
	    	public void handleMessage(Message msg){
	    		super.handleMessage(msg);
	    		switch (msg.what){
	    		// TODO 1启动连接 2 发送请求 3发送Tag -1 获取并addView
	    		case 1:
	    			Log.v("Connected", "true");
	    			Toast.makeText(getApplicationContext(), "连接服务器成功", 1500).show();
	    			break;
	    		case 2:
	    			Log.v("Sent", "true");
	    			Toast.makeText(getApplicationContext(), "组队请求已发送", 1500).show();
	    			break;
	    		case 3:
	    			Log.v("Sent", "true");
	    			break;
	    		case 4:
	    			Log.v("Receive","true");
	    			
	    		    break;
	    		
	    	}
	      }
	  }
}
