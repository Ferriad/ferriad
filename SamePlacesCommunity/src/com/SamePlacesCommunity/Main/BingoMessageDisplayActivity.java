package com.SamePlacesCommunity.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.SamePlacesCommunity.Main.MatchPageActivity.MyHandler;
import com.example.thirdapp.R;
import com.example.thirdapp.R.layout;
import com.example.thirdapp.R.menu;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


public class BingoMessageDisplayActivity extends Activity {

	private ListView bingomessagedisplay;
	private static List<Map<String,Object>> list;
	
	//information
	private String tag;
	private String information;
	private String username;
	private String userid;
	private String[] atfriend;
	
	//NetworkModule
	private static final int PORT = 7714;
	private static final String SERVERADD = "10.14.4.163";
	private Socket s;
	private DataOutputStream dos;
	private DataInputStream dis;
	private MyHandler myhandler;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bingo_message_display);
		init();
	}

	private void init() {
		// TODO Auto-generated method stub
		Intent i = getIntent();
		tag = i.getStringExtra("TAGS");
		userid = i.getStringExtra("USERID");
		username = i.getStringExtra("USERNAME");
		atfriend = i.getStringArrayExtra("ATFRIEND");
		String atfriendinfo = Extractatfriend(atfriend);
		bingomessagedisplay = (ListView)findViewById(R.id.BingoMessageDisplay);
//		setData(tag, information, username);
		list = new ArrayList<Map<String , Object>>();
		myhandler = new MyHandler();
		setData(tag,tag+"\0"+atfriendinfo,"发布人："+username);
		setData("保定真人CS大赛","保定真人CS中心是位于满城的一家集军旅文化和娱乐于一体的娱乐休闲度假村...@余烨 @王炜康 @苏晓宇","发布用户：殷子沛");
		setData("汉丽轩烤肉保定店","汉丽轩烤肉保定店是北京汉丽轩烤肉连锁超市在保定的第一家分店...@张璇@崔璨@彭柯","发布用户：路明儿●koran");
		setData("环保定自行车骑行活动","星期六下午两点半，华电二校门口，环保定骑行活动！要来的小伙伴猛戳报名！！","发布用户：柯菲儿");
		addView();
		
	}

	private String Extractatfriend(String[] atfriend) {
		// TODO Auto-generated method stub
		String atfriendinfo="";
		for(int i=0;i<atfriend.length;i++){
			if(atfriend[i]!=null&&atfriend[i]!=""){
				atfriendinfo+="@"+atfriend[i];
			}
		}
		return atfriendinfo;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bingo_message_display, menu);
		return true;
	}
	public void addView() {
    	SimpleAdapter adapter = new SimpleAdapter(
 			   this,getData(),
 			   R.layout.new_bingo_message_adapter_listview,
				   new String[]{"TAGS","INFORMATION","USERNAMES"},
				   new int[]{R.id.BingoDisPlayTag,R.id.BingoMessageDisplay,R.id.BingoDisplayUserName}
 			   );
 
    bingomessagedisplay.setAdapter(adapter);
 	
 	
 	bingomessagedisplay.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
			
			LinearLayout ll =(LinearLayout) bingomessagedisplay.getChildAt(position);
			TextView t = (TextView)ll.findViewById(R.id.BingoDisplayUserName);
			

			// TODO Auto-generated method stub
			
		}
		 
	});
	}
	private List<Map<String , Object>> getData() {
		return list;
	}

	private void setData(String tags, String information, String username){
	    HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("USERNAMES",username);
		map.put("TAGS", tags);
		map.put("INFORMATION",information);
		list.add(map);
    }
	 @Override
	    protected void onDestroy() {
	     //clear the list after activity destroyed 
		 list.clear();
	     super.onDestroy();
	    }
	 private void StartPopKeepAlive() {
			
		  Runnable PopKeepAliveRunnable = new Runnable() {
				
				@Override
				public void run() {
					while(true){
					// TODO StartPopThread
					if(!s.isConnected()){
						Connect();
						try {
							HeartPop();
							Thread.sleep(10000);
						    } catch (IOException e) {
							
							e.printStackTrace();
						    } catch (InterruptedException e) {
						
						    e.printStackTrace();
							}
						
					    }else{
					    	try {
								HeartPop();
								Thread.sleep(10000);
							} catch (IOException e) {
								
								e.printStackTrace();
							} catch (InterruptedException e) {
								
								e.printStackTrace();
							}
							
					    }
					BingoMessageDisplayActivity.this.myhandler.sendEmptyMessage(2);
				    }
				}
			};
     Thread PopKeepAlive = new Thread(PopKeepAliveRunnable);
     PopKeepAlive.start();
	}

	private void StartConnectThread() {
		
		 Runnable ConnectThreadRunnable = new Runnable() {
				
				@Override
				public void run() {
					// TODO StartConnectThread
					Connect();
					
				}
			};
			Thread ConnectThread = new Thread(ConnectThreadRunnable);
			ConnectThread.start();
	}

	/*
	   * 线程的run被执行完之后线程自动销毁，会被回收掉*/
	  public void ConnectAndSendTagForMatch(String tag) throws IOException {
			Connect();
			SendTag(tag);
	    }
<<<<<<< HEAD
	  public boolean isNetWorkAvai(){
		  
		    ConnectivityManager cm=(ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
		    NetworkInfo ni= (NetworkInfo)cm.getActiveNetworkInfo();
		    NetworkInfo ni_wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		    boolean iswificon=ni_wifi.isConnected();
		    if(ni!=null){
		    boolean ismobileco = (ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_CDMA||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_EDGE||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_GPRS||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_LTE);
	        return(ismobileco||iswificon);
	        }else{
	        	return  iswificon;
	        }
		    
	  } 
   public void Connect(){
 	   
	        if(isNetWorkAvai()){
=======
   public void Connect(){
 	    ConnectivityManager cm=(ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
			NetworkInfo ni_mobile=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	        boolean ismobilecon=ni_mobile.isConnected();
	        NetworkInfo ni_wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	        boolean iswificon=ni_wifi.isConnected();
	        
	        if(ismobilecon||iswificon){
>>>>>>> e2858b2f1935b06f340b3f1c35841a4e1b97fc2b
	        
	        	try {
		        	s= new Socket();
		        	SocketAddress sa = new InetSocketAddress(SERVERADD, PORT);
		        	s.connect(sa, 3000);
		        
		            if(!s.isConnected()){
		            	Toast.makeText(this, "服务器没有响应，连接失败", 1500).show();
		            }
		            else if(s.isConnected()){
		            	dos = new DataOutputStream (s.getOutputStream());
		            	dis = new DataInputStream (s.getInputStream());
		            	
		            }
		         }catch (UnknownHostException e) {
		              System.out.println("服务器未找到");
		              Toast.makeText(this,"服务器未找到，连接超时",1500).show();
		              e.printStackTrace();
		          }catch (SocketTimeoutException  e) {
		              System.out.println("连接超时");
		              Toast.makeText(this,"服务器正在维护中，连接超时",1500).show();
		              e.printStackTrace();
		         }catch (IOException e) {
		              System.out.println("锟紹锟斤拷失锟斤拷");
		              e.printStackTrace();
		         }
		        }else{
		        	Toast.makeText(this, "你的网络好像有问题哦，请检查网络设置", 1500).show();
		        	}
   }
   
   
	  public void SendTag(String tag) throws IOException {
		// TODO Auto-generated method stub
		    Log.v("SendTagCalled","true");
		    if(s.isConnected())
			dos.writeUTF(tag);
		    else Log.v("SocketState", "false");
	}
	  
	  public void HeartPop() throws IOException{
		  if(s.isConnected())
		    dos.writeInt(1);
		  else Log.v("SocketState", "false");
	  }
	  class MyHandler extends Handler{
	    	public MyHandler(){
	    		
	    	}
	    	public MyHandler(Looper L){
	    		super(L);
	    	}
	    	@Override
	    	
	    	public void handleMessage(Message msg){
	    		super.handleMessage(msg);
	    		switch (msg.what){
	    		// TODO 1启动连接 2 心跳 3发送Tag -1 获取并addView
	    		case 1:
	    			Log.v("Connected", "true");
	    			break;
	    		case 2:
	    			Log.v("Poped", "true");
	    			break;
	    		case 3:
	    			Log.v("Sent", "true");
	    			break;
	    		case -1:
	    			
	    		    break;
	    		
	    	}
	      }
	  }

}
