package com.SamePlacesCommunity.Main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import cn.jpush.android.api.TagAliasCallback;

import com.SamePlacesCommunity.Main.TagPushActivity.MyHandler;
import com.example.thirdapp.R;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class SetLoAndPriActivity<Main> extends Activity implements OnClickListener {
	private RadioGroup infoStateSum;
	private RadioGroup.OnCheckedChangeListener mChangeRadio;
	private RadioGroup infoPlace;
	private RadioGroup.OnCheckedChangeListener mChangeRadio2;
	private ImageView select_at_friend;
	private ImageView send_bingo_ultimate;
	private EditText tags_after_trans_TextView;
	//network
	private static final int PORT = 7714;
	private static final String SERVERADD = "10.14.4.163";
	private Socket s;
	private DataOutputStream dos;
	private DataInputStream dis;
	private MyHandler myhandler;
	//priate information
	private int infovis;
	private int placestate;
	private String location;
	private PersonInfoManager pim;
	private String tag[];
	private String tag_after_compress[];
	private String tag_after_trans;
	private String tag_match;
	private String[] id_array={"1","2"};
	private final static String[] PREPAREDCOMPARED={"北京古玩街","上海大娘水饺","深圳墨轩KTV","保定汉丽轩"}; 
	private boolean ReceiveFlag;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_lo_and_pri);
		init();
	}
	@Override
	protected void onDestroy(){
		 try {
			 if(s!=null&&s.isConnected())
				s.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("服务器错误");
			}
		super.onDestroy();
	}

	private void init() {
		Intent i = getIntent();
		tag = i.getStringArrayExtra("TAGS");
		tag_after_compress = CompressTag(tag);
		Bundle ib = GetUserInfo();
		String username = ib.getString("Name");
		String userid = ib.getString("Userid");
		String userlocation = ib.getString("Location");
		ReceiveFlag = true;
		pim = new PersonInfoManager(username, userid, userlocation);
		myhandler = new MyHandler();
		tag_after_trans = TransTags(tag);
		tags_after_trans_TextView = (EditText)findViewById(R.id.tags_after_trans);
		tags_after_trans_TextView.setText(tag_after_trans);
		
		 send_bingo_ultimate = (ImageView)findViewById(R.id.SendBingoUltimate);
		 send_bingo_ultimate.setOnClickListener(this);
		 select_at_friend  = (ImageView)findViewById(R.id.selectvisible);
		 select_at_friend.setOnClickListener(this);
		 infoStateSum = (RadioGroup)findViewById(R.id.InfoStateSum);
		 infoPlace = (RadioGroup)findViewById(R.id.infoplace);
		 infoStateSum.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch(checkedId){
	        	case R.id.InfoState1:
	        		infovis = 1;
	        		
	        		break;
	        	case R.id.InfoState2:
	        		infovis = 0;
	        		
	        		break;
	        	
	        	}
			}
		});
		 infoPlace.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch(checkedId){
	        	case R.id.PlaceState1:
	        		placestate = 1;
	        		break;
	        	case R.id.PlaceState2:
	        		placestate = 0;
	        		break;
	        	case R.id.PlaceState3:
	        		placestate = -1;
	        		break;
	        	}
			}
		});
		 tag_match = Compare(tag_after_compress,PREPAREDCOMPARED);
		 if(tag_match!=""){
			 
			 dialog(tag_match,null);
		 };
		 /***
		  * dialog
		  * ******/
		// TODO dialog
//		 StartConnectThread();
		 
	}

	private Bundle GetUserInfo() {
		// TODO Auto-generated method stub
		  SharedPreferences sharedpreferences = this.getSharedPreferences(TagPushActivity.PREFERENCE_NAME,TagPushActivity.MODE);
		  String name = sharedpreferences.getString("Name", "NAN");
	      String userid = sharedpreferences.getString("UserId", "NAN");
	      String location = sharedpreferences.getString("Location", "NAN");
	      Bundle b = new Bundle();
	      b.putString("Name", name);
	      b.putString("UserId", userid);
	      b.putString("Location", location);
	      return b;
	}
	private String Compare(String[] tag_after_compress2,
			final String[] preparedcompared2) {
		String tagreturn="";
		// TODO Auto-generated method stub
		for(int i = 0 ;i<tag_after_compress2.length;i++){
			for(int j = 0;j<preparedcompared2.length;j++){
				if(tag_after_compress2[i].equals(preparedcompared2[j])){
					tagreturn =  tag_after_compress2[i];
					}
			}
		}
		return tagreturn;
	}

	private String[] CompressTag(String[] tag2) {
		// TODO Auto-generated method stub
		int count = 0;
		for(int i=0;i<tag2.length;i++)
			if(tag2[i]!=null&&tag2[i]!="")count++;
		
		String[] TagAfterCompress = new String[count];
		int j = 0;
			for(int i=0;i<tag2.length;i++)
				if(tag2[i]!=null&&tag2[i]!=""&&j<TagAfterCompress.length){
					TagAfterCompress[j]=tag2[i];
					j++;
				};
		return TagAfterCompress;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.set_lo_and_pri, menu);
		return true;
	}
	private void SendBingoRequest(String tag[], String username, String userid,int infovisibility) {
		// TODO Auto-generated method stub
		Bundle b = new Bundle();
		b.putStringArray("TAGS", tag);
		b.putString("USERNAME", username);
		b.putString("USERID", userid);
	}
	private void dialog(String tags, String[] ids){
		
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("提示");
		builder.setMessage("找到了相同标签:"+tags+"等");
		
		builder.setPositiveButton("进去看看！", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				JumpToActivity(3);
			    SetLoAndPriActivity.this.finish();
			}
	
		});
		builder.setNegativeButton("自己发布", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
//				SetLoAndPriActivity.this.finish();
			}
		});
		builder.create().show();
	}
	private void JumpToActivity(int i) {
		// TODO Auto-generated method stub
		switch (i){
			case 1:
				String[] atfriend = {""};
	        	tag_after_trans=tags_after_trans_TextView.getText().toString();
	        	Intent i1 = new Intent(getApplicationContext(), BingoMessageDisplayActivity.class);
	        	i1.putExtra("choosedfriendposition","");
				i1.putExtra("choosedfriendnames", "");
	        	i1.putExtra("TAGS", tag_after_trans);
	        	i1.putExtra("USERNAME", pim.username);
	        	i1.putExtra("USERID", pim.userid);
	        	i1.putExtra("ATFRIEND", atfriend);
	        	startActivity(i1);
				break;
			case 2:
				tag_after_trans=tags_after_trans_TextView.getText().toString();
	        	Intent i2 = new Intent(getApplicationContext(), ChooseAtFriend.class);
	        	i2.putExtra("TAGS", tag_after_trans);
	        	i2.putExtra("USERNAME", pim.username);
	        	i2.putExtra("USERID", pim.userid);
	        	startActivity(i2);
			break;
			case 3:
				Intent i3 = new Intent(getApplicationContext(), MatchPageActivity.class);
				String temp = id_array[0];
				i3.putExtra("INFOID", temp);
			    startActivity(i3);
				break;
	    }
    }
    private String TransTags(String tag []){
    	String TagsAfterTrans = "";
    	for(int i=0;i<tag.length;i++){
    		if(tag[i]!=null)
    			TagsAfterTrans+=tag[i]+" ";
    	}
    	return TagsAfterTrans;
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

        case R.id.SendBingoUltimate:
        	StartSendTagThread(tag_match,pim.username,pim.userid,infovis,location);
        	JumpToActivity(1);
        	SetLoAndPriActivity.this.finish();
        	break;
        case R.id.selectvisible:
        	JumpToActivity(2);
        	SetLoAndPriActivity.this.finish();
         default:
             break;
     }
	}
	private void StartSendTagThread(final String inputtag, final String username, final String userid, final int infovis2,final String location) {
		// TODO 没有匹配，发送BINGO信息
		  Runnable SendTagsThreadRunnable = new Runnable() {
              public void run() {
                  System.out.println("running!");
                  String sendbingoinfo = "SendBingoInfo#"+userid+"#"+username+"#"+inputtag+"#"+location+"#"+infovis2;
                  try {
						SendTag(sendbingoinfo);
					} catch (IOException e) {
						
						e.printStackTrace();
					}  
                  SetLoAndPriActivity.this.myhandler.sendEmptyMessage(4);
              }
          };
		  Thread SendTagsThread =new Thread (SendTagsThreadRunnable); 
		  SendTagsThread.start();
		  
	}
	private void SendBingoTagThread(final String tagaftertrans) {
		// TODO 获取是否有匹配的BINGO信息
		 Runnable SendBingoTagRunnable = new Runnable() {
             public void run() {
                 System.out.println("running!");
                 String sendbingoinfo = "MatchBingoTags#"+tagaftertrans;
                 try {
						SendTag(sendbingoinfo);
					} catch (IOException e) {
						
						e.printStackTrace();
					}  
                 SetLoAndPriActivity.this.myhandler.sendEmptyMessage(3);
             }
         };
		  Thread SendTagsThread =new Thread (SendBingoTagRunnable); 
		  SendTagsThread.start();
	}

	private void StartPopKeepAlive() {
		
		  Runnable PopKeepAliveRunnable = new Runnable() {
				
				@Override
				public void run() {
					while(true){
					// TODO StartPopThread
					 if(s==null||!s.isConnected()){
						Connect();
						try {
							HeartPop();
							Thread.sleep(10000);
						    } catch (IOException e) {
							
							e.printStackTrace();
						    } catch (InterruptedException e) {
						
						    e.printStackTrace();
							}
						
					    }else{
					    	try {
								HeartPop();
								Thread.sleep(10000);
							} catch (IOException e) {
								
								e.printStackTrace();
							} catch (InterruptedException e) {
								
								e.printStackTrace();
							}
							
					    }
					SetLoAndPriActivity.this.myhandler.sendEmptyMessage(2);
				    }
				}
			};
        Thread PopKeepAlive = new Thread(PopKeepAliveRunnable);
        PopKeepAlive.start();
	}

	private void StartConnectThread() {
		
		 Runnable ConnectThreadRunnable = new Runnable() {
				
				@Override
				public void run() {
					// TODO StartConnectThread
					Connect();
					SetLoAndPriActivity.this.myhandler.sendEmptyMessage(1);
					
				}
			};
			Thread ConnectThread = new Thread(ConnectThreadRunnable);
			ConnectThread.start();
	}

	/*
	   * 线程的run被执行完之后线程自动销毁，会被回收掉*/
	  public void ConnectAndSendTagForMatch(String tag) throws IOException {
			Connect();
			 if(s!=null&&s.isConnected())
			SendTag(tag);
	    }
	  public boolean isNetWorkAvai(){
		    ConnectivityManager cm=(ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
<<<<<<< HEAD
		    NetworkInfo ni= (NetworkInfo)cm.getActiveNetworkInfo();
		    NetworkInfo ni_wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		    boolean iswificon=ni_wifi.isConnected();
		    if(ni!=null){
		    boolean ismobileco = (ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_CDMA||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_EDGE||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_GPRS||ni.getSubtype() ==TelephonyManager.NETWORK_TYPE_LTE);
	        return(ismobileco||iswificon);
	        }else{
	        	return  iswificon;
	        }
		    
	  } 
=======
		    NetworkInfo ni_mobile=cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	        boolean ismobilecon=ni_mobile.isConnected();
	        NetworkInfo ni_wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	        boolean iswificon=ni_wifi.isConnected();
	        return(ismobilecon||iswificon);
	  }
>>>>>>> e2858b2f1935b06f340b3f1c35841a4e1b97fc2b
      public void Connect(){
    	     if(isNetWorkAvai()){
	        
	        	try {
		        	s= new Socket();
		        	SocketAddress sa = new InetSocketAddress(SERVERADD, PORT);
		        	s.connect(sa, 3000);
		        
		            if(!s.isConnected()){
		            	Toast.makeText(this, "服务器没有响应，连接失败", 1500).show();
		            }
		            else if(s.isConnected()){
		            	dos = new DataOutputStream (s.getOutputStream());
		            	dis = new DataInputStream (s.getInputStream());
		            	
		            }
		         }catch (UnknownHostException e) {
		              System.out.println("服务器未找到");
		         
		              e.printStackTrace();
		          }catch (SocketTimeoutException  e) {
		              System.out.println("连接超时");
		              
		              e.printStackTrace();
		         }catch (IOException e) {
		              System.out.println("锟紹锟斤拷失锟斤拷");
		              e.printStackTrace();
		         }
		        }else{
		        	Toast.makeText(this, "你的网络好像有问题哦，请检查网络设置", 1500).show();
		        	}
      }
      
	  public void SendTag(String tag) throws IOException {
		// TODO Auto-generated method stub
		    Log.v("SendTagCalled","true");
		    if(s!=null&&s.isConnected())
			{dos.writeUTF(tag); dos.flush();}
		    else Log.v("SocketState", "false");
	}
	  private void ReceiveReturnMessageThread(){
		  
		 
		  if(s!=null&&s.isConnected()){
			Runnable ReceiveReturnThread = new Runnable() {
		        public void run() {
		        	while(ReceiveFlag){
		            System.out.println("running!");
		             ReceiveMsg();
		             
		        	}
		        }
		    };  
		    Thread temp = new Thread(ReceiveReturnThread);
		    temp.start();
		    }else{
		    	Toast.makeText(getApplicationContext(), "无法连接服务器", 1500).show();
		    }
		}
	    
	  private void ReceiveMsg() {
	        if (s!=null&&s.isConnected()) {
	            try {
	            	String reMsg = "";
	                while ((reMsg = dis.readUTF()) != null) {
	                    System.out.println(reMsg);
	                    if (reMsg.startsWith("ReturnFeeds")) {
	                    	String id[]=reMsg.split("#");//返回的用户bingo信息的ID号
	                    	id_array = id.clone();
	                    	boolean F=true;
	          
	                        try {
	                            Message m = new Message();
	                            m.what = -1;
	                            
	                            SetLoAndPriActivity.this.myhandler.sendMessage(m);                          
	                            Thread.sleep(1000);
	                        } catch (InterruptedException e) {
	                            // TODO Auto-generated catch block
	                            e.printStackTrace();
	                        }
	                    }else{
	                    	SetLoAndPriActivity.this.myhandler.sendEmptyMessage(-2);  
	                    }
	                }
	            } catch (SocketException e) {
	                // TODO: handle exception
	                System.out.println("exit!");
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }

	        }else{
	        	Message m = new Message();
	        	m.what=-2;
	        	SetLoAndPriActivity.this.myhandler.sendMessage(m);
	        }
	    
	  }
	  
	  public void HeartPop() throws IOException{
		  if(s.isConnected())
		    dos.writeInt(1);
		  else Log.v("SocketState", "false");
	  }
	  class MyHandler extends Handler{
	    	public MyHandler(){
	    		
	    	}
	    	public MyHandler(Looper L){
	    		super(L);
	    	}
	    	@Override
	    	public void handleMessage(Message msg){
	    		super.handleMessage(msg);
	    		switch (msg.what){
	    		// TODO 1启动连接 2 心跳 3发送Tag -1获取并显示对话框 －2获取失败
	    		case 1:
	    			Log.v("Connected", "true");
	    			SendBingoTagThread(tag_after_trans);
	    			break;
	    		case 2:
	    			Log.v("Poped", "true");
	    			break;
	    		case 3:
	    			Log.v("Sent", "true");
	    			ReceiveReturnMessageThread();
	    			break;
	    		case -1:
	    			Log.v("Receive","true");
	    		    dialog(id_array[1],id_array);
	    		    break;
	    		case -2:
	    			Log.v("ReceiveNull", "true");
	    			Toast.makeText(getApplicationContext(), "Bingo失败啦，自己发布吧", 1500).show();
	    			break;
	    		
	    	  }
	      }
			
	 }
}
