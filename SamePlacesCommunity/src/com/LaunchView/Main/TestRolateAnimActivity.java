package com.LaunchView.Main;

import com.SamePlacesCommunity.Main.BingoMessageDisplayActivity;
import com.clock.dk.Wdm;
import com.example.thirdapp.R;

import com.renn.rennsdk.RennClient;
import com.renn.rennsdk.RennClient.LoginListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class TestRolateAnimActivity extends Activity {
    /** Called when the activity is first created. */
	MyImageView Feeds;
	MyImageView Friends;
	MyImageView Chat;
	MyImageView VersionInformation;
//	private Button loginBtn;
//	private Button logoutBtn;
	private RennClient rennClient;
	private Wdm wdm;
	
	
	
	private static final String APP_ID = ConstantsSum.RENRENAPP_ID;

	private static final String API_KEY = ConstantsSum.RENREN_API_KEY;

	private static final String SECRET_KEY = ConstantsSum.RENREN_SECRET_KEY;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        rennClient = RennClient.getInstance(this);
		rennClient.init(APP_ID, API_KEY, SECRET_KEY);
		rennClient
				.setScope("read_user_blog read_user_photo read_user_status read_user_album "
						+ "read_user_feed read_user_notification read_user_request "
						+ "read_user_comment read_user_share publish_blog publish_share "
						+ "publish_checkin send_request operate_like "
						+ "send_notification photo_upload status_update create_album "
						+ "publish_comment publish_feed");
		
		rennClient.setTokenType("bearer");
        setContentView(R.layout.main);
        initView();
        	
        
       
        
        
        Feeds.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(TestRolateAnimActivity.this, "正在跳转", 1000).show();
				Intent intentSwitch = new Intent(TestRolateAnimActivity.this,com.SamePlacesCommunity.Main.FeedServiceActivity.class); 
				startActivity(intentSwitch);
				System.out.println("1");
			}
		});
        
        
      
        Friends.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intentSwitch = new Intent(TestRolateAnimActivity.this,com.SamePlacesCommunity.Main.UserServiceActivity.class); 
				startActivity(intentSwitch);
				System.out.println("2");
			}
		});
        
        
//        Chat.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Intent intentSwitch = new Intent(TestRolateAnimActivity.this,com.SamePlacesCommunity.Main.MatchPageActivity.class); 
//				startActivity(intentSwitch);
//				System.out.println("1");
//			}
//		});
       
       
        
//        VersionInformation.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Intent intentSwitch = new Intent(TestRolateAnimActivity.this,com.SamePlacesCommunity.Main.TagPushActivity.class); 
//				startActivity(intentSwitch);
//				System.out.println("4");
//			}
//		});
       //这里绑定登陆
        
        
        
    }
    @Override
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	        	    
	        	  	ResetZeroAds();
	        	  	wdm.showExit(this);
	            return false;
	        }
	        return false;
	    }
    private void ResetZeroAds() {
    	// TODO Auto-generated method stub
        	 wdm = Wdm.getInstance(getApplicationContext(),ConstantsSum.ADVERTISEMENTAPP_ID);
    	    wdm.init(getApplicationContext());
    	
    }
	private void initView() {
		// TODO Auto-generated method stu
//		loginBtn = (Button) findViewById(R.id.login_btn);
//		
//		logoutBtn = (Button) findViewById(R.id.logout_btn);
		
		Feeds=(MyImageView) findViewById(R.id.c_joke);
		Friends=(MyImageView) findViewById(R.id.c_idea);
		Chat=(MyImageView) findViewById(R.id.c_constellation);
		VersionInformation=(MyImageView) findViewById(R.id.c_recommend);
		ResetZeroAds();

}
}